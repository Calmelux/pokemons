import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { ReactQueryConfigProvider } from 'react-query';
import ErrorBoundary from '@components/ErrorBoundary';
import ErrorView from '@components/ErrorView';
import ContextProvider from '@context';
import Routes from '@routes';

const queryConfig = {
  throwOnError: true,
  refetchAllOnWindowFocus: true,
  refetchOnMount: true,
  retry: 3,
};

function App() {
  return (
    <ErrorBoundary render={() => <ErrorView />}>
      <ReactQueryConfigProvider config={queryConfig}>
        <ContextProvider>
          <Router>
            <Routes />
          </Router>
        </ContextProvider>
      </ReactQueryConfigProvider>
    </ErrorBoundary>
  );
}

export default App;
