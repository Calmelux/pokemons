import React, { useContext } from 'react';
import { node, string } from 'prop-types';
import { useHistory } from 'react-router-dom';

import { css } from '@emotion/css';

import { PopupContext } from '@context/popup';

const Header = props => {
  const { children, bg } = props;
  const history = useHistory();
  const { closePopup, display } = useContext(PopupContext);
  const currentPath = history?.location?.pathname;

  const goHome = () => {
    if (display) {
      closePopup();
    }
    history.push('/all-pokemon');
  };

  return (
    <>
      <div
        className={css`
          position: fixed;
          display: flex;
          right: 0;
          top: 0;
          width: 100%;
          padding: 15px;
          z-index: 999;
          background-color: ${bg};
        `}
      >
        <img
          alt=""
          src="https://res.cloudinary.com/nothingnes/image/upload/v1620274287/pokemon-23_emas1f.svg"
          className={css`
            width: 10rem;
            height: auto;
            position: absolute;
            margin: 15px;
            left: 0;
            top: 0;
            :hover {
              cursor: pointer;
            }
            visibility: ${currentPath === '/' ? 'hidden' : 'visible'};
          `}
          onClick={() => goHome()}
        />
        <div
          className={css`
            position: absolute;
            right: 0;
            margin: 15px;
          `}
        >
        </div>
      </div>
      {children}
    </>
  );
};

Header.defaultProps = {
  bg: null,
  children: null,
};

Header.propTypes = {
  bg: string,
  children: node,
};

export default Header;
