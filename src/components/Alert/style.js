import styled from '@emotion/styled';

const AlertContainer = styled('div')`
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: ${props => props.bg_color};
  height: 50vh;
  width: 50vw;
  padding: 30px;
  border-radius: 8px;
  box-shadow: 0px 0px 10px 4px rgba(179, 179, 179, 0.75);
  -webkit-box-shadow: 0px 0px 10px 4px rgba(179, 179, 179, 0.75);
  -moz-box-shadow: 0px 0px 10px 4px rgba(179, 179, 179, 0.75);
  ...props;
`;

const CloseIcon = styled('img')`
  position: absolute;
  top: 0;
  right: 0;
  height: 50px;
  width: 50px;
  margin: 20px;
  ...props;
`;

const AlertImage = styled('img')`
  height: 230px;
  width: 230px;
  margin: 20px;
  @media only screen and (max-width: 768px) {
    height: 150px;
    width: 150px;
  }
  ...props;
`;

const Message = styled('p')`
  color: ${props => props.color};
  font-size: ${props => props.size};
  text-align: ${props => props.align};
  @media only screen and (max-width: 768px) {
    font-size: ${props => props.sm_size};
  }
  ...props;
`;

export {
  AlertContainer, CloseIcon, AlertImage, Message
};
