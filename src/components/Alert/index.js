import React, { useContext } from 'react';
import { func, string, bool } from 'prop-types';
import { ThemeContext } from '@context/theme';

import { AlertContainer, CloseIcon, AlertImage, Message } from './style.js';

const Alert = ({ text, image, closeAlert, isOpen }) => {
  const { theme } = useContext(ThemeContext);

  const isLight = theme === 'light';

  return (
    <>
      {isOpen ? (
        <AlertContainer bg_color={isLight ? '#EDEDED' : '#566573'} data-testid="alert-id">
          <CloseIcon
            alt=""
            src="https://res.cloudinary.com/nothingnes/image/upload/v1620361386/pokemon_moltres_icon-icons.com_67518_qivb49.svg"
            onClick={closeAlert}
          />
          <AlertImage alt="" src={image}></AlertImage>
          <Message color={isLight ? '#282c34' : '#FFF'} align="center" size="30px">
            {text}
          </Message>
        </AlertContainer>
      ) : null}
    </>
  );
};

Alert.propTypes = {
  closeAlert: func.isRequired,
  image: string.isRequired,
  isOpen: bool.isRequired,
  text: string.isRequired,
};

export default Alert;
