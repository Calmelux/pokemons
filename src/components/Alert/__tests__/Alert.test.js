import React from 'react';
import Alert from '@components/Alert';
import { render } from '@testing-library/react';

//Matches Snapshot Test
test('<Alert /> matches snapshot!', () => {
  const component = render(
    <Alert
      isOpen
      text="Psyduck is confused! Pokemon name cannot be identical with what you already have."
      image="https://res.cloudinary.com/nothingnes/image/upload/v1620406913/Psyduck_icon-icons.com_67371_r8tlxg.png"
      closeAlert={() => {}}
    />,
  );
  expect(component.container).toMatchSnapshot();
});

test('<Alert /> should render', () => {
  const div = document.createElement('div');
  const { getByTestId } = render(
    <Alert
      isOpen
      text="Hello"
      image="https://res.cloudinary.com/nothingnes/image/upload/v1620406913/Psyduck_icon-icons.com_67371_r8tlxg.png"
      closeAlert={() => {}}
    />,
    div,
  );

  const content = getByTestId('alert-id');
  expect(content.textContent).toBe('Hello');
});
