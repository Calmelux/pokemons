import React from 'react';
import { func } from 'prop-types';
import { BackButtonContainer, Button } from './style.js';

const BackButton = ({ goTo }) => {
  return (
    <BackButtonContainer>
      <Button
        alt="back-button"
        src="https://res.cloudinary.com/nothingnes/image/upload/a_-90/v1620357873/Pokemon_Go_Arrow_icon-icons.com_67522_daylkj.svg"
        onClick={goTo}
      />
    </BackButtonContainer>
  );
};

BackButton.propTypes = {
  goTo: func,
};

BackButton.defaultProps = {
  goTo: undefined,
};

export default BackButton;
