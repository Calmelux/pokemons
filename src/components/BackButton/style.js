import styled from '@emotion/styled';

const BackButtonContainer = styled('div')`
  position: fixed;
  bottom: 0;
  left: 0;
  width: min-content;
  height: auto;
  ...props;
`;

const Button = styled('img')`
  width: 40px;
  height: 40px;
  margin: 20px;
  cursor: pointer;
  transform: rotateZ(-90deg);
  ...props;
`;

export { BackButtonContainer, Button };
