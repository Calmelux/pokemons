import React from 'react';
import BackButton from '@components/BackButton';
import { render } from '@testing-library/react';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

//Matches Snapshot Test
test('<BackButton /> matches snapshot!', () => {
  const component = render(<BackButton />);
  expect(component.container).toMatchSnapshot();
});
