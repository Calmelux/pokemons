import styled from '@emotion/styled';

const CatchResultContainer = styled('div')`
  position: fixed;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.bg_color};
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  ...props;
`;

const PopupImage = styled('img')`
  height: 200px;
  width: 200px;
  margin: 30px 0;

  ...props;
`;

const IconImage = styled('img')`
  height: ${props => props.height};
  width: ${props => props.width};
  align-self: ${props => props.align};
  margin: 10px 30px;
  :hover {
    cursor: pointer;
  }
  ...props;
`;

const IconContainer = styled('div')`
  position: fixed;
  display: flex;
  flex-direction: column;
  right: 0;
  ...props;
`;

const Textfield = styled('input')`
  height: 60px;
  width: 230px;
  font-size: 30px;
  border: 4px solid #252525;
  border-radius: 10px;
  font-family: 'Bebas Neue', cursive !important;
  margin: 5px 0;
  ...props;
`;

const Button = styled('button')`
  height: 60px;
  width: 230px;
  font-size: 30px;
  border: 4px solid #252525;
  border-radius: 10px;
  font-family: 'Bebas Neue', cursive !important;
  background-color: #f0b91c;
  margin: 5px 0;
`;

const Message = styled('p')`
  font-family: 'Bebas Neue', cursive;
  font-size: ${props => props.size};
  margin: ${props => props.margin};
  @media only screen and (max-width: 991px) {
    font-size: ${props => props.md_size};
  }
  @media only screen and (max-width: 768px) {
    font-size: ${props => props.sm_size};
  }
  text-align: center;
  color: ${props => props.color};

  ...props;
`;

export {
  CatchResultContainer, PopupImage, Message, IconImage, Textfield, Button, IconContainer
};
