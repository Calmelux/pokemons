import React, { useContext, useState, useEffect } from 'react';
import { object } from 'prop-types';

import { PopupContext } from '@context/popup';
import { ThemeContext } from '@context/theme';
import { PokemonContext } from '@context/pokemon';

import Alert from '@components/Alert';

import { CatchResultContainer, PopupImage, Message, Textfield, Button } from './style.js';

const CatchResult = props => {
  const { history } = props;
  const { display, text, icon, variant, closePopup } = useContext(PopupContext);
  const { theme } = useContext(ThemeContext);
  const { allPokemon, selectedPokemon, handleAddPokemon } = useContext(PokemonContext);
  const [pokemon, setPokemon] = useState({
  });
  const [alert, setAlert] = useState(false);

  useEffect(() => {
    if (selectedPokemon) {
      setPokemon(selectedPokemon);
    }
    return;
  }, [selectedPokemon]);

  const handleChange = (name, value) => {
    setPokemon(prev => ({
      ...prev,
      [name]: value?.toLowerCase(),
    }));
  };

  const savePokemonToList = () => {
    const tempArray = allPokemon.filter(item => item.name === pokemon.name);
    if (tempArray.length !== 0) {
      setAlert(true);
    } else {
      handleAddPokemon(pokemon);
      closePopup();
      history.push('/all-pokemon');
    }
  };

  const handleKeyPress = e => {
    if (e.key === 'Enter') {
      savePokemonToList();
    }
  };

  const isLight = theme === 'light';
  return (
    <>
      {display ? (
        <CatchResultContainer bg_color={isLight ? '#FFF' : '#282c34'}>
          <PopupImage alt="" src={icon} />
          <Message sm_size="30px" md_size="40px" size="50px" color={isLight ? '#282c34' : '#FFF'} align="center">
            {text}
          </Message>
          {variant === 'success' && (
            <>
              <Textfield
                type="text"
                placeholder="Pokemon Name"
                label="Pokemon Name"
                value={pokemon.name}
                onChange={e => handleChange('name', e.target.value)}
                onKeyPress={e => handleKeyPress(e)}
              ></Textfield>
              <Button onClick={() => savePokemonToList()} onKeyPress={e => handleKeyPress(e)} disabled={!pokemon.name}>
                Save Pokemon
              </Button>
            </>
          )}
          <Alert
            isOpen={alert}
            text="Psyduck is confused! Pokemon name cannot be identical with what you already have."
            image="https://res.cloudinary.com/nothingnes/image/upload/v1620406913/Psyduck_icon-icons.com_67371_r8tlxg.png"
            closeAlert={() => setAlert(false)}
          />
        </CatchResultContainer>
      ) : null}
    </>
  );
};

CatchResult.propTypes = {
  history: object.isRequired,
};

export default CatchResult;
