import React, { useContext } from 'react';
import { node } from 'prop-types';
import { css } from '@emotion/css';

import Header from '@components/DefaultHeader';
import CatchResult from '@components/CatchResult';

import { ThemeContext } from '@context/theme';

import { useHistory } from 'react-router-dom';

const Layout = ({ children }) => {
  const { theme } = useContext(ThemeContext);
  const history = useHistory();

  return (
    <div
      className={css`
        background-color: ${theme === 'dark' ? '#282c34' : '#fff'};
        transition: 1s;
      `}
    >
      <Header bg="transparent"> {children}</Header>
      <CatchResult history={history} />
    </div>
  );
};

Layout.propTypes = {
  children: node.isRequired,
};

export default Layout;
