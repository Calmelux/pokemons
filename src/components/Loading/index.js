import React, { useContext } from 'react';
import { bool } from 'prop-types';

import { ThemeContext } from '@context/theme';
import { LoadingContainer, LoadingImage, LoadingText } from './style.js';

const Loading = ({ isLoading }) => {
  const { theme } = useContext(ThemeContext);
  const isLight = theme === 'light';

  return (
    <>
      {isLoading && (
        <LoadingContainer data-testid="loading-id" bg_color={!isLight ? '#282c34' : '#FFF'}>
          <LoadingImage
            alt=""
            src="https://res.cloudinary.com/nothingnes/image/upload/v1620275952/pokemon-go-new-loading-screen-2021-1-939x2048-2_rzubw1.jpg"
          />
          <LoadingText color={isLight ? '#282c34' : '#FFF'}>Loading...</LoadingText>
        </LoadingContainer>
      )}
    </>
  );
};

Loading.propTypes = {
  isLoading: bool,
};

Loading.defaultProps = {
  isLoading: false,
};

export default Loading;
