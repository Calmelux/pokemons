import styled from '@emotion/styled';

const LoadingContainer = styled('div')`
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100%;
  z-index: 9191;
  background-color: ${props => props.bg_color};
  ...props;
`;

const LoadingImage = styled('img')`
  height: max-content;
  width: 80%;
  ...props;
`;

const LoadingText = styled('p')`
  font-size: 30px;
  font-family: 'Bebas Neue', cursive;
  color: ${props => props.color};
  ...props;
`;

export {
  LoadingContainer, LoadingImage, LoadingText
};
