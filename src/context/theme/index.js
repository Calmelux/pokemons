import React, { useState, useEffect } from 'react';
import { object } from 'prop-types';

export const ThemeContext = React.createContext({
  theme: '',
});

export const ThemeProvider = props => {
  const { children } = props;
  const [theme, setTheme] = useState(null);
  const currentTheme = sessionStorage.getItem('th');

  useEffect(() => {
    if (currentTheme) {
      setTheme(currentTheme);
    } else {
      setTheme('light');
    }
  }, [currentTheme]);

  return <ThemeContext.Provider value={{
    theme: theme, setTheme, currentTheme
  }}>{children}</ThemeContext.Provider>;
};

ThemeProvider.propTypes = {
  children: object.isRequired,
};
