import React from 'react';
import { node } from 'prop-types';
import { ThemeProvider } from '@context/theme';
import { PopupProvider } from '@context/popup';
import { PokemonProvider } from '@context/pokemon';

function ContextProvider({ children }) {
  return (
    <ThemeProvider>
      <PokemonProvider>
        <PopupProvider>{children}</PopupProvider>
      </PokemonProvider>
    </ThemeProvider>
  );
}

ContextProvider.propTypes = {
  children: node.isRequired,
};

export default ContextProvider;
