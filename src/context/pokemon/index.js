import React, { useState, useEffect } from 'react';
import { object } from 'prop-types';

export const PokemonContext = React.createContext({
  allPokemon: [],
  handleAddPokemon: () => { },
  handleReleasePokemon: () => { },
});

export const PokemonProvider = props => {
  const { children } = props;
  const [allPokemon, setAllPokemon] = useState([]);
  const [selectedPokemon, setSelectedPokemon] = useState({
  });
  const localPokemon = JSON.parse(localStorage.getItem('ap'));

  useEffect(() => {
    if (localPokemon) {
      setAllPokemon(localPokemon);
    } else {
      let tempArray = [];
      localStorage.setItem('ap', JSON.stringify(tempArray));
    }
    return () => {
      setAllPokemon([]);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleAddPokemon = async data => {
    let tempArray = allPokemon;
    tempArray.push({
      ...data, id: allPokemon.length
    });
    localStorage.setItem('ap', JSON.stringify(tempArray));
    setAllPokemon(tempArray);
  };

  const handleReleasePokemon = async data => {
    let tempArray = [];
    tempArray = allPokemon.filter(item => {
      return item?.id !== data?.id;
    });
    localStorage.setItem('ap', JSON.stringify(tempArray));
    setAllPokemon(tempArray);
  };

  return (
    <PokemonContext.Provider
      value={{
        allPokemon: allPokemon,
        handleAddPokemon,
        handleReleasePokemon,
        selectedPokemon: selectedPokemon,
        setSelectedPokemon,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
};

PokemonProvider.propTypes = {
  children: object.isRequired,
};
