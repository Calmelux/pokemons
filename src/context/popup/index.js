import React, { createContext, useCallback, useEffect, useState } from 'react';
import { node } from 'prop-types';

const defaultProps = {
  variant: 'success',
  duration: 2000,
  text: '',
  display: false,
  icon: '',
};

export const PopupContext = createContext(defaultProps);

export const PopupProvider = ({ children }) => {
  const [text, setText] = useState(defaultProps.text);
  const [display, setDisplay] = useState(defaultProps.display);
  const [duration, setDuration] = useState(defaultProps.duration);
  const [variant, setVariant] = useState(defaultProps.variant);
  const [icon, setIcon] = useState(defaultProps.duration);

  useEffect(() => {
    const element = document.querySelector('html');
    if (display) {
      element.style.overflowY = 'hidden';
    } else {
      element.style.overflowY = 'scroll';
    }
  }, [display]);

  const showPopup = useCallback(({ text = '', variant = '', duration = 2000, display = true }) => {
    setDisplay(display);
    setDuration(duration);
    setVariant(variant);
    setText(text);
    if (variant === 'success') {
      setIcon('https://res.cloudinary.com/nothingnes/image/upload/v1620379670/Gotcha_icon-icons.com_67553_kmsj3m.svg');
    } else if (variant === 'failed') {
      setIcon(
        'https://res.cloudinary.com/nothingnes/image/upload/v1620460567/Open_Pokeball_icon-icons.com_67538_w8kcok.ico',
      );
    }
  }, []);

  const closePopup = useCallback(() => {
    setText('');
    setDisplay(false);
    setDuration(0);
  }, []);

  useEffect(() => {
    let timeout;

    if (duration) {
      if (variant === 'failed') {
        setTimeout(closePopup, duration);
      }
    }

    return () => {
      if (timeout) {
        clearTimeout(timeout);
      }
    };
  }, [closePopup, duration, variant]);

  return (
    <PopupContext.Provider
      value={{
        showPopup,
        closePopup,
        duration: duration,
        text: text,
        display: display,
        icon: icon,
        variant: variant,
      }}
    >
      {children}
    </PopupContext.Provider>
  );
};

PopupProvider.propTypes = {
  children: node.isRequired,
};
