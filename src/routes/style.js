import styled from '@emotion/styled';
import { Row, Col } from 'reactstrap';

const ContentRow = styled(Row)`
  ...props;
`;

const ColumnCenter = styled(Col)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: ${props => props.margin}
  text-align: ${props => props.text_align};
  ...props;
`;

const ColumnLeft = styled(Col)`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  flex-direction: column;
  text-align: ${props => props.text_align};
  ...props;
`;

const P = styled('p')`
  color: ${props => props.color};
  width: ${props => props.width};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  cursor: ${props => props.cursor};
  text-align: ${props => props.text_align};
  @media only screen and (max-width: 991px) {
    font-size: ${props => props.md_size} !important;
  }
  @media only screen and (max-width: 768px) {
    font-size: ${props => props.sm_size} !important;
  }
  ...props;
`;

const Span = styled('span')`
  color: ${props => props.color};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  @media only screen and(max-width: 768px) {
    font-size: ${props => props.sm_size};
  }
  ...props;
`;

const H1 = styled('h1')`
  color: ${props => props.color};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  @media only screen and(max-width: 768px) {
    font-size: ${props => props.sm_size};
  }
  ...props;
`;

const Div = styled('div')`
  margin: ${props => props.margin};
  ...props;
`;

const CollectionText = styled('div')`
  font-family: 'Bebas Neue', cursive !important;
  color: ${props => props.color};
  margin: ${props => props.margin};
  font-size: ${props => props.size};
  ...props;
`;

const Img = styled('img')`
  height=${props => props.height};
  width=${props => props.width};
  margin=${props => props.margin};
 
  ...props;
`;

const Container = styled('div')`
  min-height: 100vh;
  height: 100%;
  width: 100%;
  padding: 100px 0;
  overflow-x: hidden;
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  animation: fadein 1s linear;
  transition: 1s;
  ...props;
`;

const PokemonImg = styled('img')`
  width: ${props => props.size};
  height: ${props => props.size};
  @media only screen and (max-width: 991px) {
    width: ${props => props.lg_size};
    height: ${props => props.lg_size};
  }
  @media only screen and (max-width: 768px) {
    width: ${props => props.md_size};
    height: ${props => props.md_size};
  }
  @media only screen and (max-width: 428px) {
    width: ${props => props.sm_size};
    height: ${props => props.sm_size};
  }
  ...props;
`;

const PokeballImg = styled('img')`
  position: relative;
  height: 70px;
  width: 70px;
  @keyframes rotatebounce {
    0% {
      transform: translateY(-10%) rotateZ(0);
    }
    50% {
      transform: translateY(10%);
    }
    100% {
      transform: translateY(-10%) rotateZ(360deg);
    }
  }
  animation: rotatebounce 2s infinite;
  margin: 30px 0;
  :hover {
    cursor: pointer;
  }
  ...props;
`;

const ImageWrapper = styled('div')`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin: 20px 0;
  z-index: 2;
  @media only screen and (max-width: 768px) {
    flex-direction: column;
  }
  ...props;
`;

const HoverableText = styled('span')`
  color: ${props => props.color};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  text-decoration: ${props => props.text_decoration};
  text-align: ${props => props.text_align};
  @media only screen and(max-width: 768px) {
    font-size: ${props => props.sm_size};
  }
  width: max-content;
  :hover {
    opacity: 0.8;
    cursor: pointer;
  }
`;

export {
  P,
  ColumnCenter,
  ColumnLeft,
  ContentRow,
  Span,
  H1,
  Div,
  Img,
  Container,
  PokemonImg,
  PokeballImg,
  CollectionText,
  ImageWrapper,
  HoverableText,
};
