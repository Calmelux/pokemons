import React, { useEffect } from 'react';
import { Route, Switch, useHistory, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';

import Layout from '@components/Layout';
import HomeComponent from '@routes/Home';
import MyPokemonList from '@routes/MyPokemonList';
import RegionList from '@routes/RegionList';
import PokemonDetail from '@routes/PokemonDetail';
import PokemonList from '@routes/PokemonList';
import AllPokemon from '@routes/AllPokemon';

const Routes = () => {
  const history = useHistory();

  useEffect(() => {
    history.push('', '');
    window.addEventListener('popstate', () => {
      history.go(1);
    });
  }, [history]);

  return (
    <Layout>
      <Switch>
        <Container>
          <Route path="/" exact component={HomeComponent} />
          <Route path="/all-pokemon" exact component={AllPokemon} />
          <Route path="/region" exact component={RegionList} />
          <Route path="/all-pokemon/:pokemonName" component={PokemonDetail} />
          <Route path="/pokemon-list" exact component={PokemonList} />
          <Route path="/my-pokemon" exact component={MyPokemonList} />
          <Redirect to="/" />
        </Container>
      </Switch>
    </Layout>
  );
};

export default Routes;
