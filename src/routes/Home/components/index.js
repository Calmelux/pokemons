import React, { useContext } from 'react';
import { useHistory } from 'react-router-dom';

import { ThemeContext } from '@context/theme';

import { HomeContainer, FadingText, PokemonLogo } from './style.js';

const Home = () => {
  const history = useHistory();
  const { theme } = useContext(ThemeContext);
  const isLight = theme === 'light';
  const localPokemon = JSON.parse(localStorage.getItem('ap'));

  const toAllPokemon = () => {
    if (!localPokemon) {
      const tempArray = [];
      localStorage.setItem('ap', JSON.stringify(tempArray));
    }
    return history.push('/all-pokemon');
  };

  return (
    <HomeContainer data-testid="home-container" onClick={() => toAllPokemon()}>
      <PokemonLogo
        alt="pokemon-logo"
        data-testid="pokemon-button"
        src="https://res.cloudinary.com/nothingnes/image/upload/v1620274287/pokemon-23_emas1f.svg"
        onClick={() => toAllPokemon()}
      />
      <FadingText size="23px" color={isLight ? '#282c34' : '#FFF'}>
        &apos;TAP/CLICK&apos; TO START
      </FadingText>
    </HomeContainer>
  );
};

export default Home;
