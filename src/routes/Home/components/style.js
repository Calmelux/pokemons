import styled from '@emotion/styled';

const HomeContainer = styled('div')`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 100vh !important;
  overflow: hidden !important;
  min-height: 100vh;
  width: 100%;
  padding: 100px 0;
  overflow-x: hidden;
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  animation: fadein 1s linear;
  transition: 1s;
  ...props;
`;

const PokemonLogo = styled('img')`
  width: 40rem;
  height: auto;
  @media only screen and (min-width: 901px) {
    :hover {
      cursor: pointer;
      transition: 0.3s;
      width: 32rem;
    }
  }
  @media only screen and (max-width: 900px) {
    width: 40rem;
  }
  @media only screen and (max-width: 768px) {
    width: 30rem;
  }
  @media only screen and (max-width: 500px) {
    width: 20rem;
  }
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  animation-delay: 1s;
  animation: fadein 1s linear;
  transition: 0.3s;
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
`;

const FadingText = styled('p')`
  color: ${props => props.color};
  width: ${props => props.width};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  cursor: ${props => props.cursor};
  text-align: ${props => props.text_align};
  @media only screen and (max-width: 991px) {
    font-size: ${props => props.md_size} !important;
  }
  @media only screen and (max-width: 768px) {
    font-size: ${props => props.sm_size} !important;
  }
  @keyframes fadeinout {
    0% {
      opacity: 1;
    }
    50% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  }
  animation: fadeinout 3s infinite;
`;

export {
  HomeContainer, PokemonLogo, FadingText
};
