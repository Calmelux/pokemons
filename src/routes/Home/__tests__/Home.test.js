import React from 'react';
import { render, cleanup } from '@testing-library/react';
import Home from '@routes/Home/components';
import '@testing-library/jest-dom/extend-expect';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('[Home] - Test', () => {
  afterEach(cleanup);

  test('<Home /> Should be able to render', () => {
    const { getByTestId } = render(<Home />);

    expect(getByTestId('home-container')).toBeVisible();
  });

  test('<Home /> Should match snapshot', () => {
    const component = render(<Home />);
    expect(component.container).toMatchSnapshot();
  });

  test('<Home /> click logo should have alt and src', () => {
    const component = render(<Home />);
    const logo = component.getByRole('img');

    expect(logo).toHaveAttribute(
      'src',
      'https://res.cloudinary.com/nothingnes/image/upload/v1620274287/pokemon-23_emas1f.svg',
    );
    expect(logo).toHaveAttribute('alt', 'pokemon-logo');
  });
});
