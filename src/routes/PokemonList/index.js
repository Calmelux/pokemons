import loadable from '@loadable/component';

const errorLoading = err => console.log('PokemonList page loading failed!', err);

const PokemonList = loadable(
  () => import(/* webpackChunkName: "pokemon-list" */ '@routes/PokemonList/components').catch(errorLoading),
  {
    ssr: true,
  },
);

export default PokemonList;
