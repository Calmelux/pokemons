import styled from '@emotion/styled';

const PokemonListContainer = styled('div')`
  z-index: 2;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  @media only screen and (max-width: 768px) {
    justify-content: center;
    flex-direction: column;
  }
  margin: ${props => props.margin};
  ...props;
`;

const PokemonNames = styled('p')`
  color: ${props => props.color};
  width: ${props => props.width};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  cursor: ${props => props.cursor};
  text-align: ${props => props.text_align};
  @media only screen and (max-width: 991px) {
    font-size: ${props => props.md_size} !important;
  }
  @media only screen and (max-width: 768px) {
    font-size: ${props => props.sm_size} !important;
    text-align: center;
  }
  :hover {
    color: #ffc300;
    transition: 0.3s;
  }
  ...props;
`;

const PinPointImg = styled('img')`
  width: 100px;
  height: 100px;
  margin: 20px 30px;
  @keyframes bounce {
    0% {
      transform: translateY(-20px);
    }
    50% {
      transform: translateY(0px);
    }
    100% {
      transform: translateY(-20px);
    }
  }
  animation: bounce 1s infinite;
  ...props;
`;

export {
  PokemonListContainer, PokemonNames, PinPointImg
};
