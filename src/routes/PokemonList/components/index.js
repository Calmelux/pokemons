import React, { useContext } from 'react';
import { useQuery } from 'react-query';
import { useHistory } from 'react-router-dom';
import { object } from 'prop-types';
import Skeleton from '@material-ui/lab/Skeleton';

import { ThemeContext } from '@context/theme';
import { PokemonContext } from '@context/pokemon';

import { API_URL } from '@constants';

import useFetch from '@hooks/useFetch';

import Loading from '@components/Loading';
import BackButton from '@components/BackButton';

import { P, Container, ImageWrapper, HoverableText, ContentRow, ColumnCenter } from '@routes/style.js';
import { PokemonNames, PokemonListContainer, PinPointImg } from './style.js';

const PokemonList = props => {
  const { theme } = useContext(ThemeContext);
  const { allPokemon } = useContext(PokemonContext);
  const { state } = props.location || {
  };
  const history = useHistory();
  const isLight = theme === 'light';

  const region = JSON.parse(sessionStorage.getItem('regd'));

  const { data, status } = useQuery(['pokedex-list'], useFetch(`${API_URL}/pokedex/${region || state}`));
  const pokemonList = data?.pokemon_entries || [];

  return (
    <Container data-testid="pokemon-list" id="pokemon-list">
      <Loading isLoading={status !== 'success'} />
      <PokemonListContainer>
        <ImageWrapper>
          <PinPointImg
            alt=""
            src="https://res.cloudinary.com/nothingnes/image/upload/v1620380829/Location_icon-icons.com_67549_vkjmhp.svg"
          ></PinPointImg>
          <P size="100px" sm_size="60px" color={isLight ? '#282c34' : '#FFF'} margin="0">
            {region || state}
          </P>
        </ImageWrapper>

        <HoverableText
          size="30px"
          color={isLight ? '#282c34' : '#FFF'}
          text_decoration="underline"
          onClick={() => history.push('/my-pokemon', '/pokemon-list')}
        >
          Pokemon Owned: {allPokemon.length}
        </HoverableText>
      </PokemonListContainer>

      <br />
      {data?.descriptions ? (
        <>
          <P size="50px" sm_size="30px" color={isLight ? '#282c34' : '#FFF'} text_align="center" margin="30px 0 50px 0">
            {data?.descriptions ? data?.descriptions[data?.descriptions?.length - 1]?.description : ''}
          </P>
          <ContentRow>
            {pokemonList?.map((item, index) => {
              return (
                <ColumnCenter
                  key={index}
                  sm="6"
                  md="4"
                  lg="3"
                  onClick={() => history.push(`/all-pokemon/${item?.pokemon_species?.name}`, '/pokemon-list')}
                >
                  <PokemonNames size="40px" color={isLight ? '#282c34' : '#FFF'} cursor="pointer">
                    {item?.pokemon_species?.name}
                  </PokemonNames>
                </ColumnCenter>
              );
            })}
          </ContentRow>
        </>
      ) : (
        <Skeleton variant="rect" width="100%" height="100vh" animation="wave" />
      )}
      <BackButton goTo={() => history.push('/region')} />
    </Container>
  );
};

PokemonList.propTypes = {
  location: object,
};

PokemonList.defaultProps = {
  location: {
  },
};

export default PokemonList;
