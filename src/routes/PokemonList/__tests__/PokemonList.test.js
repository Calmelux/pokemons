import React from 'react';
import { render, cleanup } from '@testing-library/react';
import PokemonList from '@routes/PokemonList/components';
import '@testing-library/jest-dom/extend-expect';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('[PokemonList] - Test', () => {
  afterEach(cleanup);

  test('<PokemonList /> Should be able to render Loading', () => {
    const { getByTestId } = render(<PokemonList />);

    expect(getByTestId('pokemon-list')).toBeVisible();
  });

  test('<PokemonList /> Should match snapshot', () => {
    const component = render(<PokemonList />);
    expect(component.container).toMatchSnapshot();
  });
});
