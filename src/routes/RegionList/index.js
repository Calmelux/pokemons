import loadable from '@loadable/component';

const errorLoading = err => console.log('RegionList page loading failed!', err);

const RegionList = loadable(
  () => import(/* webpackChunkName: "region-list" */ '@routes/RegionList/components').catch(errorLoading),
  {
    ssr: true,
  },
);

export default RegionList;
