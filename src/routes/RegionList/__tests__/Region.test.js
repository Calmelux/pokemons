import React from 'react';
import { render, cleanup } from '@testing-library/react';
import RegionList from '@routes/RegionList/components';
import '@testing-library/jest-dom/extend-expect';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('[RegionList] - Test', () => {
  afterEach(cleanup);

  test('<RegionList /> Should be able to render', () => {
    const { getByTestId } = render(<RegionList />);

    expect(getByTestId('region-container')).toBeVisible();
  });

  test('<RegionList /> Should match snapshot', () => {
    const component = render(<RegionList />);
    expect(component.container).toMatchSnapshot();
  });
});
