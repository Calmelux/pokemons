import React, { useContext } from 'react';
import { css } from '@emotion/css';
import { useQuery } from 'react-query';
import { useHistory } from 'react-router-dom';
import Skeleton from '@material-ui/lab/Skeleton';

import { ThemeContext } from '@context/theme';

import { API_URL } from '@constants';

import useFetch from '@hooks/useFetch';

import Loading from '@components/Loading';
import BackButton from '@components/BackButton';
import { P, Span, Container, Img, ImageWrapper } from '@routes/style.js';
import { ContentRow, PokemonList, GreenWallpaper } from './style.js';

const RegionList = () => {
  const { theme } = useContext(ThemeContext);
  const history = useHistory();
  const isLight = theme === 'light';

  const { data, status } = useQuery(['pokedex'], useFetch(`${API_URL}/pokedex`));
  const regionList = data?.results || [];

  const toPokemonList = data => {
    sessionStorage.setItem('regd', JSON.stringify(data));
    history.push('/pokemon-list', data);
  };

  return (
    <Container data-testid="region-container" id="#region">
      <Loading isLoading={status !== 'success' || !regionList} />
      <ImageWrapper>
        <GreenWallpaper
          bg_color={
            isLight
              ? 'linear-gradient(90deg, rgba(0,255,173,1) 0%, rgba(9,121,40,1) 35%, rgba(0,98,73,1) 100%)'
              : 'linear-gradient(90deg, rgba(0, 98, 73, 1) 0%, rgba(9, 121, 40, 1) 35%, rgba(0, 255, 173, 1) 100%)'
          }
        />
        <Img
          alt=""
          src="https://res.cloudinary.com/nothingnes/image/upload/v1620379527/Pokemon_Location_icon-icons.com_67519_krrufl.svg"
          className={css`
            z-index: 1;
          `}
        ></Img>
        <P
          size="60px"
          sm_size="50px"
          color={isLight ? '#282c34' : '#FFF'}
          margin="0 20px"
          className={css`
            z-index: 1;
          `}
        >
          POKEMON REGIONS
        </P>
      </ImageWrapper>

      <ContentRow>
        {regionList ? (
          <>
            {' '}
            {regionList?.map((item, index) => {
              return (
                <PokemonList
                  key={index}
                  sm="6"
                  md="6"
                  lg="3"
                  bg_color={isLight ? '#2C3E50' : '#EAEDED'}
                  onClick={() => toPokemonList(item?.name)}
                >
                  <Span size="40px" color={isLight ? '#FFF' : '#282c34'}>
                    {item?.name}
                  </Span>
                </PokemonList>
              );
            })}{' '}
          </>
        ) : (
          <Skeleton variant="rect" width={210} height={118} />
        )}
      </ContentRow>
      <BackButton goTo={() => history.push('/all-pokemon')} />
    </Container>
  );
};

export default RegionList;
