import styled from '@emotion/styled';
import { Row, Col } from 'reactstrap';

const ContentRow = styled(Row)`
  display: flex;
  justify-content: center;
  position: relative;
  height: 100%;
  ...props;
`;

const PokemonList = styled(Col)`
  text-align: ${props => props.text_align};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: ${props => props.bg_color};
  height: 100px;
  margin: 20px;
  transition: 0.7s;
  :hover {
    cursor: pointer;
    transition: 0.7s;
    background-color: #05b365;
  }
  ...props;
`;

const GreenWallpaper = styled('div')`
  clip-path: polygon(0 0, 100% 0%, 100% 34%, 0 62%);
  background: ${props => props.bg_color};
  height: 100vh;
  width: 100%;
  position: fixed;
  left: 0;
  z-index: 0;
  transition: 1s;
`;

export {
  PokemonList, ContentRow, GreenWallpaper
};
