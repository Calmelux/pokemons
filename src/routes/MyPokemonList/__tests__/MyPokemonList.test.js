import React from 'react';
import { render, cleanup } from '@testing-library/react';
import MyPokemonList from '@routes/MyPokemonList/components';
import '@testing-library/jest-dom/extend-expect';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('[MyPokemonList] - Test', () => {
  afterEach(cleanup);

  test('<MyPokemonList /> Should be able to render', () => {
    const { getByTestId } = render(<MyPokemonList />);

    expect(getByTestId('my-pokemon-id')).toBeVisible();
  });

  test('<MyPokemonList /> Should match snapshot', () => {
    const component = render(<MyPokemonList />);
    expect(component.container).toMatchSnapshot();
  });
});
