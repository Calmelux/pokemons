import loadable from '@loadable/component';

const errorLoading = err => console.log('MyPokemonList page loading failed!', err);

const MyPokemonList = loadable(
  () => import(/* webpackChunkName: "my-pokemon" */ '@routes/MyPokemonList/components').catch(errorLoading),
  {
    ssr: true,
  },
);

export default MyPokemonList;
