import React, { useContext } from 'react';
import { css } from '@emotion/css';
import { useHistory } from 'react-router-dom';
import { object } from 'prop-types';

import { ThemeContext } from '@context/theme';
import { PokemonContext } from '@context/pokemon';

import BackButton from '@components/BackButton';

import { Span, P, Container, HoverableText, PokemonImg, ContentRow, ColumnCenter, Img } from '@routes/style.js';
import { NoPokemonContainer } from './style.js';

const MyPokemonList = ({ location }) => {
  const history = useHistory();
  const { state } = location;
  const { theme } = useContext(ThemeContext);
  const { allPokemon, handleReleasePokemon } = useContext(PokemonContext);
  const isLight = theme === 'light';

  if (allPokemon?.length < 1) {
    return (
      <NoPokemonContainer data-testid="my-pokemon-id">
        <P color={isLight ? '#282c34' : '#FFF'} size="40px" sm_size="25px">
          YOU DON&apos;T HAVE ANY POKEMON.{' '}
          <HoverableText onClick={() => history.push('/region')} text_decoration="underline">
            GO CATCH SOME!
          </HoverableText>
        </P>
      </NoPokemonContainer>
    );
  }

  return (
    <Container data-testid="my-pokemon-id">
      <P color={isLight ? '#282c34' : '#FFF'} size="150px" md_size="80px" sm_size="70px">
        My Pokemon
      </P>
      <ContentRow
        className={css`
          transition: 1s;
        `}
      >
        {allPokemon.map((item, index) => {
          return (
            <ColumnCenter key={index} sm="12" md="6" lg="25" border_color={isLight ? '#282c34' : '#FFF'}>
              <Img
                alt=""
                src="https://res.cloudinary.com/nothingnes/image/upload/v1620460567/Open_Pokeball_icon-icons.com_67538_w8kcok.ico"
                height="30px"
                width="30px"
                margin="20px 0"
                cursor="pointer"
                className={css`
                  cursor: pointer;
                `}
                onClick={() => handleReleasePokemon(item)}
              ></Img>
              <PokemonImg
                alt=""
                size="250px"
                src={
                  item.image || 'https://res.cloudinary.com/nothingnes/image/upload/v1620274287/pokemon-23_emas1f.svg'
                }
              ></PokemonImg>
              <P color={isLight ? '#282c34' : '#FFF'} size="30px" text_align="left" width="100%">
                name: {item.name}
              </P>
              <P key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" text_align="left" width="100%">
                type:
                {item?.types?.map((item, index) => {
                  return (
                    <Span key={index}>
                      {item.type.name}
                      {index + 1 === item?.types?.length ? '' : ','}
                    </Span>
                  );
                })}
              </P>
              <P key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" text_align="left" width="100%">
                ability:
                {item?.abilities?.map((item, index) => {
                  return (
                    <Span key={index}>
                      {item.ability.name}
                      {index + 1 === item?.abilities?.length ? '' : ','}{' '}
                    </Span>
                  );
                })}
              </P>
              <P color={isLight ? '#282c34' : '#FFF'} size="30px" text_align="left" width="100%">
                species: {item.species}
              </P>
            </ColumnCenter>
          );
        })}
      </ContentRow>
      <BackButton goTo={() => history.push(state)} />
    </Container>
  );
};

MyPokemonList.propTypes = {
  location: object,
};

MyPokemonList.defaultProps = {
};
export default MyPokemonList;
