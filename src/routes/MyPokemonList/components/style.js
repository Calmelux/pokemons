import styled from '@emotion/styled';

const NoPokemonContainer = styled('div')`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  width: 100% ...props;
`;

export { NoPokemonContainer };
