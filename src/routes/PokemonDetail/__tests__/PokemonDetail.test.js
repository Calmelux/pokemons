import React from 'react';
import { render, cleanup } from '@testing-library/react';
import PokemonDetail from '@routes/PokemonDetail/components';
import '@testing-library/jest-dom/extend-expect';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('[PokemonDetail] - Test', () => {
  afterEach(cleanup);

  test('<PokemonDetail /> Should be able to render', () => {
    const { getByTestId } = render(<PokemonDetail />);

    expect(getByTestId('detail-container')).toBeVisible();
  });

  test('<PokemonDetail /> Should match snapshot', () => {
    const component = render(<PokemonDetail />);
    expect(component.container).toMatchSnapshot();
  });
});
