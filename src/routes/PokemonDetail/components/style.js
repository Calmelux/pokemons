import styled from '@emotion/styled';
import { Col } from 'reactstrap';

const BallContainer = styled('div')`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin: ${props => props.margin};
  ...props;
`;

const Modal = styled('div')`
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 0;
  left: 0;
  height: 100vh;
  width: 100%;
  ::after {
    content: '';
    position: fixed;
    height: 100vh;
    width: 100%;
    background-color: black;
    opacity: 0.5;
  }

  ...props;
`;

const ModalBox = styled('div')`
  z-index: 11111;
  position: relative;
  height: 80%;
  width: 90%;
  background-color: ${props => props.bg};
  transition: 1s;
  @keyframes fadeintop {
    from {
      opacity: 0;
      transform: translateY(-25%);
    }
    to {
      opacity: 1;
      transform: translateY(0);
    }
  }
  padding: 20px 40px;
  overflow-y: scroll !important;
  ::-webkit-scrollbar {
    display: none;
  }
  animation: fadeintop 0.3s linear;
  ...props;
`;

const CloseIcon = styled('img')`
  position: absolute;
  top: 0;
  right: 0;
  height: 50px;
  width: 50px;
  margin: 20px;

  :hover {
    transition: 0.3s;
    cursor: pointer;
    height: 55px;
    width: 55px;
  }
  ...props;
`;

const CatchPokemonDiv = styled('div')`
  position: fixed;
  display: flex;
  align-items: center;
  flex-direction: column;
  bottom: 0;
  left: 50%;
  margin-left: -50px;
  padding: 20px;
  ...props;
`;

const PokemonName = styled('p')`
  color: ${props => props.color};
  font-size: 100px;
  font-family: 'Bebas Neue', cursive !important;
  margin: ${props => props.margin};
  cursor: ${props => props.cursor};
  @media only screen and (max-width: 991px) {
    font-size: 80px;
  }
  @media only screen and (max-width: 768px) {
    font-size: 75px;
  }
  ...props;
`;

const MovesDiv = styled('div')`
  min-height: 200px;
  height: 100%;
  margin: 60px 0px;
  ...props;
`;

const PokemonColumn = styled(Col)`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin: ${props => props.margin};
  text-align: ${props => props.text_align};
  ...props;
`;

const PokeballCollection = styled('div')`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: space-evenly;
  bottom: 0;
  right: 0;
  background-color: ${props => props.bg};
  height: 150px;
  width: 50%;
  margin: 20px;
  transform: ${props => props.opened};
  border-radius: 5px;
  box-shadow: 1px 0px 9px 0px rgba(0, 0, 0, 0.75);
  -webkit-box-shadow: 1px 0px 9px 0px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 1px 0px 9px 0px rgba(0, 0, 0, 0.75);
  transition: 0.5s;
  :hover {
    transition: 0.5s;
    cursor: pointer;
  }
  @media only screen and (max-width: 768px) {
    width: 90%;
  }
  ...props;
`;

export {
  BallContainer,
  Modal,
  ModalBox,
  CloseIcon,
  CatchPokemonDiv,
  PokemonName,
  MovesDiv,
  PokeballCollection,
  PokemonColumn,
};
