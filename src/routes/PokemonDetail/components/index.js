import React, { useState, useContext, useEffect, useCallback } from 'react';
import { css } from '@emotion/css';
import { useQuery } from 'react-query';
import { object } from 'prop-types';
import { useHistory } from 'react-router-dom';
import Skeleton from '@material-ui/lab/Skeleton';
import { isEmpty } from 'lodash';

import { ThemeContext } from '@context/theme';
import { PokemonContext } from '@context/pokemon';
import { PopupContext } from '@context/popup';

import { API_URL } from '@constants';

import useFetch from '@hooks/useFetch';

import pokeBalls from '@helpers/pokeBalls.js';

import Loading from '@components/Loading';
import BackButton from '@components/BackButton';
import { P, Span, Div, Container, PokemonImg, PokeballImg, ContentRow, ColumnLeft } from '@routes/style.js';
import {
  BallContainer,
  Modal,
  ModalBox,
  CloseIcon,
  CatchPokemonDiv,
  MovesDiv,
  PokemonName,
  PokemonColumn,
  PokeballCollection,
} from './style.js';

const generatePokemon = ({ pokemonName }) => `${API_URL}/pokemon/${pokemonName}`;
const generateType = ({ selectedType }) => `${API_URL}/type/${selectedType}`;
const generateAbility = ({ selectedAbility }) => `${API_URL}/ability/${selectedAbility}`;

const PokemonDetail = props => {
  const { theme } = useContext(ThemeContext);
  const { showPopup } = useContext(PopupContext);

  const { setSelectedPokemon } = useContext(PokemonContext);
  const { state } = props.location;

  const { pokemonName } = props.match.params || {
  };
  const [selectedType, setSelectedType] = useState(null);
  const [selectedAbility, setSelectedAbility] = useState(null);
  const [currentBall, setCurrentBall] = useState(pokeBalls[0]); //set default value to Standard ball from pokeBall array
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [damageRelation, setDamageRelation] = useState({
  });
  const [effectEntries, setEffectEntries] = useState([]);
  const [pokemonData, setPokemonData] = useState({
  });
  const history = useHistory();

  const isLight = theme === 'light';

  const { data: pokemonDetail, status: pokemonDetailStatus, refetch: getPokemonDetail } = useQuery({
    queryKey: [`getPokemon-${pokemonName}`],
    queryFn: useFetch(generatePokemon),
    variables: {
      pokemonName: pokemonName || '',
    },
    config: {
      manual: true,
    },
  });

  useEffect(() => {
    getPokemonDetail();
  }, [getPokemonDetail]);

  useEffect(() => {
    const lockOverflow = document.querySelector('html');
    if (isModalOpen) {
      lockOverflow.style.overflowY = 'hidden';
    } else lockOverflow.style.overflowY = 'scroll';
  }, [getPokemonDetail, isModalOpen]);

  const { data: pokemonAbility, status: abilityStatus, refetch: getAbilityData } = useQuery({
    queryKey: [`getAbility-${selectedAbility}`],
    queryFn: useFetch(generateAbility),
    variables: {
      selectedAbility: selectedAbility || '',
    },
    config: {
      manual: true,
    },
  });

  const { data: pokemonType, status: typeStatus, refetch: getTypeData } = useQuery({
    queryKey: [`getType-${selectedType}`],
    queryFn: useFetch(generateType),
    variables: {
      selectedType: selectedType || '',
    },
    config: {
      manual: true,
    },
  });

  const fetchSubData = useCallback(async () => {
    if (selectedType) {
      const data = await getTypeData();
      if (data) {
        setDamageRelation(data?.damage_relations);
      }
    }
    if (selectedAbility) {
      const data = await getAbilityData();
      if (data) {
        const enData = data?.effect_entries?.filter(item => item?.language?.name === 'en');
        const effectDetails = {
          effect: enData[0]?.effect, shortEffect: enData[0]?.short_effect
        };
        setEffectEntries(effectDetails);
      }
    }
  }, [getAbilityData, getTypeData, selectedAbility, selectedType]);

  useEffect(() => {
    if (pokemonDetail) {
      fetchSubData();
      setPokemonData({
        name: pokemonDetail?.name || '',
        species: pokemonDetail?.species?.name || '',
        abilities: pokemonDetail?.abilities || [],
        types: pokemonDetail?.types || [],
        weight: pokemonDetail?.weight || '',
        image: pokemonDetail?.sprites?.other?.dream_world?.front_default || '',
      });
    }
    return () => {
      setPokemonData({
      });
      fetchSubData();
    };
  }, [fetchSubData, pokemonDetail, pokemonName]);

  const reset = () => {
    setSelectedType(null);
    setSelectedAbility(null);
    setPokemonData({
    });
  };

  const openModalAndFetchData = (name, data) => {
    reset();
    if (name === 'ability') {
      setSelectedAbility(data);
    } else if (name === 'type') {
      setSelectedType(data);
    }
    setIsModalOpen(true);
  };

  const closeModal = () => {
    reset();
    setIsModalOpen(false);
  };

  const toAllPokemon = () => {
    setPokemonData({
    });
    history.push(state);
  };

  const catchPokemon = data => {
    if (Math.random() < currentBall?.catchRate) {
      showPopup({
        text: `You have caught ${data?.species} successfully!`, variant: 'success'
      });
      setSelectedPokemon(data);
    } else {
      showPopup({
        text: `${data?.species} has escaped!`, variant: 'failed'
      });
      return;
    }
  };

  return (
    <Container data-testid="detail-container">
      <Loading isLoading={pokemonDetailStatus !== 'success' || isEmpty(pokemonDetail)} />;
      {!isEmpty(pokemonDetail) ? (
        <ContentRow>
          <PokemonColumn
            sm="12"
            md="12"
            lg="6"
            className={css`
              display: flex;
              justify-content: center;
            `}
          >
            <PokemonImg
              alt=""
              src={
                pokemonData?.image ||
                'https://res.cloudinary.com/nothingnes/image/upload/v1620274287/pokemon-23_emas1f.svg'
              }
              size="400px"
              lg_size="310px"
              md_size="350px"
              sm_size="320px"
            ></PokemonImg>
          </PokemonColumn>
          <ColumnLeft sm="12" md="12" lg="6">
            <PokemonName color={isLight ? '#282c34' : '#FFF'}>{pokemonData.name}</PokemonName>
            {abilityStatus === 'success' || typeStatus === 'success' ? (
              <>
                <P color={isLight ? '#282c34' : '#FFF'} size="30px" margin="0">
                  Type:{' '}
                  {pokemonData?.types?.map((item, index) => {
                    return (
                      <Span
                        key={index}
                        color={isLight ? '#282c34' : '#FFF'}
                        onClick={() => openModalAndFetchData('type', item?.type?.name)}
                        className={css`
                          :hover {
                            cursor: pointer;
                          }
                        `}
                      >
                        {item?.type?.name || ''}
                        {index + 1 === pokemonDetail?.types?.length ? '' : ','}{' '}
                      </Span>
                    );
                  })}
                </P>
                <P color={isLight ? '#282c34' : '#FFF'} size="30px" margin="0">
                  Abilities:{' '}
                  {pokemonData?.abilities?.map((item, index) => {
                    return (
                      <Span
                        key={index}
                        color={isLight ? '#282c34' : '#FFF'}
                        onClick={() => openModalAndFetchData('ability', item?.ability?.name)}
                        className={css`
                          :hover {
                            cursor: pointer;
                          }
                        `}
                      >
                        {item?.ability?.name || ''}
                        {index + 1 === pokemonDetail?.abilities?.length ? '' : ','}{' '}
                      </Span>
                    );
                  })}
                </P>{' '}
                <P color={isLight ? '#282c34' : '#FFF'} size="30px" margin="0">
                  Species: <Span color={isLight ? '#282c34' : '#FFF'}>{pokemonData?.species}</Span>
                </P>{' '}
                <br />
                <P color={isLight ? '#282c34' : '#FFF'} size="30px" margin="0">
                  TIPS:
                  <br />
                  <Span size="20px" margin="0">
                    You can &apos;tap/click&apos; on the pokemon&apos;s type/ability to see their details!
                  </Span>
                </P>
              </>
            ) : (
              <Skeleton variant="rect" width="300px" height="200px" animation="wave" />
            )}
          </ColumnLeft>
        </ContentRow>
      ) : (
        <Skeleton variant="rect" width="100%" height="500px" animation="wave" />
      )}
      <MovesDiv>
        <Span color={isLight ? '#282c34' : '#FFF'} size="50px">
          POKEMON&apos;S MOVES
        </Span>
        <br />
        {!isEmpty(pokemonDetail) ? (
          <>
            {pokemonDetail?.moves.map((item, index) => {
              return (
                <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px">
                  {item.move?.name}
                  {index + 1 === pokemonDetail?.moves?.length ? '' : ','}{' '}
                </Span>
              );
            })}
          </>
        ) : (
          <Skeleton variant="rect" width="100%" height="200px" animation="wave" />
        )}
      </MovesDiv>
      {!isEmpty(pokemonDetail) && (
        <CatchPokemonDiv>
          <PokeballImg alt="" src={currentBall.image} onClick={() => catchPokemon(pokemonData)} />
          <Span color={isLight ? '#282c34' : '#FAFAFA'}>Catch!</Span>
          <PokeballCollection
            bg={isLight ? '#FAFAFA' : '#282c34'}
            opened={isOpen ? 'translateX(0%)' : 'translateX(95%)'}
            onClick={() => setIsOpen(!isOpen)}
          >
            {pokeBalls.map((item, index) => {
              return (
                <BallContainer key={index} onClick={() => setCurrentBall(item)}>
                  <PokeballImg alt="" src={item.image} />
                  <P color={isLight ? '#282c34' : '#FFF'} size="20px">
                    {item.name}
                  </P>
                </BallContainer>
              );
            })}
          </PokeballCollection>
        </CatchPokemonDiv>
      )}
      {isModalOpen && (
        <Modal>
          <ModalBox bg={isLight ? '#FFF' : '#282c34'}>
            {pokemonType || pokemonAbility ? (
              <>
                <CloseIcon
                  alt=""
                  src="https://res.cloudinary.com/nothingnes/image/upload/v1620361386/pokemon_moltres_icon-icons.com_67518_qivb49.svg"
                  onClick={() => closeModal()}
                ></CloseIcon>
                {selectedType ? (
                  <Div>
                    <P color={isLight ? '#282c34' : '#FFF'} size="80px" margin="20px 0">
                      {pokemonType?.name || ''}
                    </P>

                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Strong Attack Against:{' '}
                      <>
                        {damageRelation?.double_damage_to?.map((item, index) => {
                          return (
                            <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                              {item?.name}
                              {index + 1 === damageRelation?.double_damage_to?.length ? '' : ','}{' '}
                            </Span>
                          );
                        })}
                      </>
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Weak Attack Against:{' '}
                      <>
                        {damageRelation?.half_damage_to?.map((item, index) => {
                          return (
                            <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                              {item?.name}
                              {index + 1 === damageRelation?.half_damage_to?.length ? '' : ','}{' '}
                            </Span>
                          );
                        })}
                      </>
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Strong Defense Against:{' '}
                      <>
                        {damageRelation?.half_damage_from?.map((item, index) => {
                          return (
                            <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                              {item?.name}
                              {index + 1 === damageRelation?.half_damage_from?.length ? '' : ','}{' '}
                            </Span>
                          );
                        })}
                      </>
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Weak Defense Against:{' '}
                      <>
                        {damageRelation?.double_damage_from?.map((item, index) => {
                          return (
                            <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                              {item?.name}
                              {index + 1 === damageRelation?.double_damage_from?.length ? '' : ','}{' '}
                            </Span>
                          );
                        })}
                      </>
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Immune Damage From:{' '}
                      <>
                        {damageRelation?.no_damage_from?.map((item, index) => {
                          return (
                            <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                              {item?.name}
                              {index + 1 === damageRelation?.no_damage_from?.length ? '' : ','}{' '}
                            </Span>
                          );
                        })}
                      </>
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      No Damage Given To:{' '}
                      <>
                        {damageRelation?.no_damage_to?.map((item, index) => {
                          return (
                            <Span key={index} color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                              {item?.name}
                              {index + 1 === damageRelation?.no_damage_to?.length ? '' : ','}{' '}
                            </Span>
                          );
                        })}
                      </>
                    </P>
                  </Div>
                ) : (
                  <Div>
                    <P color={isLight ? '#282c34' : '#FFF'} size="80px" margin="20px 0">
                      {pokemonAbility?.name || ''}
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Effect:{' '}
                      <Span color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                        {effectEntries.effect || ''}
                      </Span>
                    </P>
                    <P size="50px" sm_size="35px" color={isLight ? '#282c34' : '#FFF'}>
                      Short Effect:{' '}
                      <Span color={isLight ? '#282c34' : '#FFF'} size="30px" sm_size="20px">
                        {effectEntries.shortEffect || ''}
                      </Span>
                    </P>
                  </Div>
                )}
              </>
            ) : (
              <Skeleton variant="rect" width="100%" height="100%" animation="wave" />
            )}
          </ModalBox>
        </Modal>
      )}
      <BackButton goTo={() => toAllPokemon()} />
    </Container>
  );
};

PokemonDetail.propTypes = {
  location: object,
  match: object,
};

PokemonDetail.defaultProps = {
  location: {
  },
  match: {
  },
};

export default PokemonDetail;
