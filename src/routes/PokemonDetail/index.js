import loadable from '@loadable/component';

const errorLoading = err => console.log('PokemonDetail page loading failed!', err);

const PokemonDetail = loadable(
  () => import(/* webpackChunkName: "pokemon-detail" */ '@routes/PokemonDetail/components').catch(errorLoading),
  {
    ssr: true,
  },
);

export default PokemonDetail;
