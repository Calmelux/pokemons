import React, { useContext, useEffect, useState, useRef } from 'react';
import { css } from '@emotion/css';
import { useHistory } from 'react-router-dom';
import { useQuery } from 'react-query';

import queryParamBuilder from '@helpers/queryParamBuilder.js';

import { API_URL } from '@constants';

import Loading from '@components/Loading';

import { ThemeContext } from '@context/theme';
import { PokemonContext } from '@context/pokemon';

import useFetch from '@hooks/useFetch';

import { P, Container, HoverableText, ContentRow, ColumnCenter, Span } from '@routes/style.js';
import {
  PokemonListContainer,
  PokemonRow,
  YellowWallpaper,
  PikachuImage,
  PaginationText,
  FloatingRegion,
} from './style.js';

const generatePokemon = ({ limit, offset }) =>
  `${API_URL}/pokemon/${queryParamBuilder({
    limit: limit, offset: offset
  })}`;

console.log(process.env);

const AllPokemon = () => {
  const history = useHistory();

  const { theme } = useContext(ThemeContext);
  const { allPokemon } = useContext(PokemonContext);

  const containerRef = useRef(document.getElementById('container_id'));

  const isLight = theme === 'light';
  const limit = 20;
  const [currentOffset, setCurrentOffset] = useState(0);

  const { data, status, refetch: getPokemonList } = useQuery({
    queryKey: ['pokemon'],
    queryFn: useFetch(generatePokemon),
    variables: {
      limit: limit,
      offset: currentOffset,
    },
    config: {
      manual: true,
    },
  });

  useEffect(() => {
    if (currentOffset >= 0) {
      getPokemonList();
    }
  }, [currentOffset, getPokemonList]);

  const nextPage = () => {
    if (data) {
      if (containerRef?.current?.getBoundingClientRect()?.y === 0) {
        const element = document.getElementById('pokemon_list');
        element.scrollIntoView();
      }
      if (data?.next) {
        setCurrentOffset(currentOffset + limit);
      }
      return;
    }
    return;
  };

  const prevPage = () => {
    if (containerRef?.current?.getBoundingClientRect()?.y === 0) {
      const element = document.getElementById('pokemon_list');
      element.scrollIntoView();
    }
    if (data?.previous) {
      setCurrentOffset(currentOffset - limit);
    }
    return;
  };

  return (
    <Container data-testid="all-pokemon-container" id="container_id" ref={containerRef}>
      <Loading isLoading={status !== 'success' || !data?.results} />
      <YellowWallpaper
        bg_color={
          isLight
            ? 'linear-gradient(90deg, rgba(255,255,103,1) 0%, rgba(255,251,26,1) 78%, rgba(255,156,0,1) 100%);'
            : 'linear-gradient(90deg, rgba(255,156,0,1) 0%, rgba(255,251,26,1) 78%, rgba(255,255,103,1) 100%)'
        }
      />
      <ContentRow>
        <ColumnCenter sm="12" md="6" lg="6">
          <P size="130px" md_size="70px" sm_size="60px" color={isLight ? '#282c34' : '#FFF'}>
            ALL POKEMON
          </P>
          <HoverableText
            size="30px"
            md_size="30px"
            sm_size="20px"
            color={isLight ? '#282c34' : '#FFF'}
            text_decoration="underline"
            text_align="right"
            onClick={() => history.push('/my-pokemon', '/all-pokemon')}
          >
            Pokemon Owned: {allPokemon.length}
          </HoverableText>
        </ColumnCenter>
        <ColumnCenter sm="12" md="6" lg="6">
          <PikachuImage
            alt=""
            src="https://res.cloudinary.com/nothingnes/image/upload/v1620533752/pikachu-pokemon_aknrxh.png"
            height="auto"
            width="100%"
          ></PikachuImage>
        </ColumnCenter>
      </ContentRow>
      <PokemonListContainer>
        <PaginationText
          color={isLight ? '#282c34' : '#FFF'}
          size="50px"
          md_size="40px"
          sm_size="35px"
          right="-30px"
          onClick={() => nextPage()}
          className={css`
            right: 0;
          `}
          margin="0 30px"
        >
          <Span>➤</Span>
        </PaginationText>
        <PokemonRow>
          {data?.results?.map((item, index) => {
            return (
              <ColumnCenter key={index} sm="12" md="6" lg="4">
                <HoverableText
                  id="pokemon_list"
                  size="50px"
                  md_size="30px"
                  sm_size="20px"
                  text_align="center"
                  onClick={() => history.push(`/all-pokemon/${item?.name}`, '/all-pokemon')}
                  color={isLight ? '#282c34' : '#FFF'}
                >
                  {item?.name}
                </HoverableText>
              </ColumnCenter>
            );
          })}
        </PokemonRow>
        <PaginationText
          color={isLight ? '#282c34' : '#FFF'}
          size="50px"
          md_size="40px"
          sm_size="35px"
          left="-30px"
          onClick={() => prevPage()}
          className={css`
            left: 0;
            transform: rotateY(180deg);
          `}
          margin="0 30px"
        >
          <Span>➤</Span>
        </PaginationText>
      </PokemonListContainer>
      <FloatingRegion
        alt=""
        src="https://res.cloudinary.com/nothingnes/image/upload/v1620379527/Pokemon_Location_icon-icons.com_67519_krrufl.svg"
        size="80px"
        md_size="60px"
        sm_size="40px"
        onClick={() => history.push('/region')}
      ></FloatingRegion>
    </Container>
  );
};

export default AllPokemon;
