import styled from '@emotion/styled';
import { Row } from 'reactstrap';

const PokemonListContainer = styled('div')`
  margin: ${props => props.margin};
  position: relative;
  margin: 50px 0;
  ...props;
`;

const PokemonRow = styled(Row)`
  display: flex;
  width: 100%;
  flex-direction: row;
  align-items: center;
  ...props;
`;

const YellowWallpaper = styled('div')`
  clip-path: polygon(0 11%, 100% 30%, 100% 60%, 0 62%);
  background: ${props => props.bg_color};
  height: 100vh;
  width: 100%;
  position: fixed;
  left: 0;
  z-index: 0;
  transition: 1s;
`;

const PikachuImage = styled('img')`
  height: ${props => props.height};
  width: ${props => props.width};
  margin: ${props => props.margin};
  @media only screen and (max-width: 991px) {
    width: 200px;
  }
  @media only screen and (max-width: 768px) {
    width: 180px;
  }
  ...props;
`;

const PaginationText = styled('p')`
  position: absolute;
  top: 50%;
  bottom: 50%;
  z-index: 9190;
  left: ${props => props.left};
  right: ${props => props.right};
  color: ${props => props.color};
  font-size: ${props => props.size};
  font-family: 'Bebas Neue', cursive;
  margin: ${props => props.margin};
  cursor: ${props => props.cursor};
  text-align: ${props => props.text_align};
  @media only screen and (max-width: 991px) {
    font-size: ${props => props.md_size} !important;
  }
  @media only screen and (max-width: 768px) {
    font-size: ${props => props.sm_size} !important;
  }
  :hover {
    cursor: pointer;
  }
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  ...props;
`;

const FloatingRegion = styled('img')`
  position: fixed;
  right: 0;
  bottom: 0;
  width: ${props => props.size};
  height: ${props => props.size};
  @media only screen and(max-width: 768px) {
    width: ${props => props.size};
    height: ${props => props.size};
  }
  @media only screen and(max-width: 768px) {
    width: ${props => props.size};
    height: ${props => props.size};
  }
  cursor: pointer;
  margin: 20px;
  @keyframes bouncing {
    0% {
      transform: translateY(0);
    }
    50% {
      transform: translateY(-25%);
    }
    100% {
      transform: translateY(0);
    }
  }
  animation: bouncing 1s infinite;
  transition: 1s;
  ...props;
`;

export {
  PokemonListContainer, PokemonRow, YellowWallpaper, PikachuImage, PaginationText, FloatingRegion
};
