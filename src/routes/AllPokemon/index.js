import loadable from '@loadable/component';

const errorLoading = err => console.log('AllPokemon page loading failed!', err);

const AllPokemon = loadable(
  () => import(/* webpackChunkName: "all-pokemon" */ '@routes/AllPokemon/components').catch(errorLoading),
  {
    ssr: true,
  },
);

export default AllPokemon;
