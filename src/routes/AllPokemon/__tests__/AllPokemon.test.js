import React from 'react';
import { render, cleanup } from '@testing-library/react';
import AllPokemon from '@routes/AllPokemon/components';
import '@testing-library/jest-dom/extend-expect';

const mockHistoryPush = jest.fn();

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useHistory: () => ({
    push: mockHistoryPush,
  }),
}));

describe('[AllPokemon] - Test', () => {
  afterEach(cleanup);

  test('<AllPokemon /> Should be able to render', () => {
    const { getByTestId } = render(<AllPokemon />);

    expect(getByTestId('all-pokemon-container')).toBeVisible();
  });

  test('<AllPokemon /> Should match snapshot', () => {
    const component = render(<AllPokemon />);
    expect(component.container).toMatchSnapshot();
  });
});
