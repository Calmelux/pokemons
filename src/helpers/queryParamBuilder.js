export default params => {
  let queryParams = '';

  for (let key in params) {
    if (!params[key]) continue;
    queryParams = queryParams.concat(`${queryParams ? '&' : '?'}${key}=${params[key]}`);
  }
  return queryParams;
};
