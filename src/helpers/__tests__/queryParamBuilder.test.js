import queryParamBuilder from '@helpers/queryParamBuilder';
import { cleanup } from '@testing-library/react';

describe('[QueryParamBuilder] - Function Test', () => {
  afterEach(cleanup);

  test('Query param builder should returns value', () => {
    const data = 10;
    const food = 'burger';
    const result = queryParamBuilder({ data: data, food: food });
    expect(result).toStrictEqual(`?data=${data}&food=${food}`);
  });

  test('Query param builder should empty data values', () => {
    const data = '';
    const food = '';
    const result = queryParamBuilder({ data: data, food: food });
    expect(result).toStrictEqual(``);
  });
});
