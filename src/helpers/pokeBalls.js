//YOU CAN ADD MORE TYPES OF POKEBALL HERE WITH DIFFERENT EFFECTS

const pokeBalls = [
  {
    id: 1,
    name: 'Standard Ball',
    alias: 'standard_ball',
    catchRate: 0.5,
    image:
      'https://res.cloudinary.com/nothingnes/image/upload/v1620292354/pokemon_go_play_game_cinema_film_movie_icon-icons.com_69163_gqm5es.svg',
  },
  {
    id: 2,
    name: 'Ultra Ball',
    alias: 'ultra_ball',
    catchRate: 0.75,
    image:
      'https://res.cloudinary.com/nothingnes/image/upload/v1620386712/pokemon_go_play_game_cinema_film_movie_3_icon-icons.com_69162_ufi5q5.svg',
  },
  // {
  //   id: 3,
  //   name: 'Master Ball',
  //   alias: 'ultra_ball',
  //   catchRate: 1,
  //   image:
  //     'https://res.cloudinary.com/nothingnes/image/upload/v1620386712/pokemon_go_play_game_cinema_film_movie_3_icon-icons.com_69162_ufi5q5.svg',
  // },
];
export default pokeBalls;
