const MUTATION_REQUEST = ['POST', 'PUT'];

const useFetch = (urlScheme, options = {
}) => {
  return (key, variables) => {
    const usedVariables = MUTATION_REQUEST.includes(options.method) ? key : variables;

    let finalUrl;

    if (typeof urlScheme === 'function') {
      finalUrl = urlScheme(usedVariables);
    } else {
      finalUrl = String(urlScheme);
    }

    const promise = fetch(finalUrl, {
      headers: {
        'Content-Type': 'application/json',
        ...(options.headers || {
        }),
      },
      ...options,
    })
      .then(res => res.json())
      .catch(error => console.log('error', error));
    return promise;
  };
};

export default useFetch;
