const path = require('path');

function resolver({ rootDir }) {
  const appSrc = `${rootDir}/src`;

  return {
    '@': path.resolve(appSrc),
    '@assets': path.resolve(appSrc, './assets/'),
    '@components': path.resolve(appSrc, './components/'),
    '@context': path.resolve(appSrc, './context/'),
    '@constants': path.resolve(appSrc, './constants/'),
    '@helpers': path.resolve(appSrc, './helpers/'),
    '@hooks': path.resolve(appSrc, './hooks/'),
    '@route-gateway': path.resolve(appSrc, './routes/index.js'),
    '@routes': path.resolve(appSrc, './routes/'),
    '@styles': path.resolve(appSrc, './styles/'),
    '@config': path.resolve(rootDir, './config/'),
  };
}

module.exports = resolver;
